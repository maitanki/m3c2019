! Lab 4, task 2

program task2
    implicit none
    ! integer :: i2
    integer, parameter :: N=5, display_iteration = 0
    real(kind=8) :: odd_sum

    odd_sum = 0.d0

!Add a do-loop which sums the first N odd integers and prints the result

call computeSum(N,odd_sum,display_iteration)
print *, 'odd_sum=',odd_sum


end program task2

!------------
!Subroutine which sums the first N odd integers
subroutine computeSum(N,odd_sum,display_iteration)
implicit none
integer, intent(in) :: N, display_iteration
integer :: i2
real(kind=8), intent(out) :: odd_sum

odd_sum = 0.d0

do i2= 1,N  ! loop from 1 to N
    odd_sum = odd_sum + 2.d0*i2-1.d0
    if (display_iteration == 1) then
        print *, 'iteration', i2, 'sum=', odd_sum
    end if
end do

end subroutine computeSum


! To compile this code:
! $ gfortran -o task2.exe lab42.f90
! To run the resulting executable: $ ./task2.exe
